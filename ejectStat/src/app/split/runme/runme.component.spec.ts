import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RunmeComponent } from './runme.component';

describe('RunmeComponent', () => {
  let component: RunmeComponent;
  let fixture: ComponentFixture<RunmeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RunmeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RunmeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
