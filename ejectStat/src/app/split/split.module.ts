import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RunmeComponent } from './runme/runme.component';
import { BrowserModule } from '@angular/platform-browser';
import { AfterComponent } from './after/after.component';

@NgModule({
  declarations: [RunmeComponent, AfterComponent],
  imports: [
    BrowserModule,
  ],
  bootstrap: [RunmeComponent, AfterComponent]
})
export class SplitModule { }
