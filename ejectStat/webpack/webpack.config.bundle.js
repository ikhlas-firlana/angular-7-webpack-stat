const BundleTracker = require('webpack-bundle-tracker');
const MergePack = require('webpack-merge');
const Entry = require('./entry.js');

/**
 * This is where you define your additional webpack configuration items to be appended to
 * the end of the webpack config.
 */
module.exports = MergePack(Entry, {
  output: {
    filename: '[hash].js',
    chunkFilename: '[id].[hash].chunk.js'
  },
  plugins: [
    new BundleTracker({
      filename: './webpack-stats.json'
    })
  ]
})