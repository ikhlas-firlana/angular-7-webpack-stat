'use strict';

const environment = (process.env.NODE_ENV ? process.env.NODE_ENV : 'bundle').trim();
if (environment === 'bundle') {
  module.exports = require('./webpack/webpack.config.bundle');
} else if (environment === 'dev') {
  module.exports = require('./webpack/webpack.config.dev');
}