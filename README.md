# angular-7-webpack-stat

extract webpack json from project

# Getting Start
```
npm install @angular/cli
```

## There are two ways for build project

using spa or using split project

### spa
```
npm run build
```
read `angular.json`. in some apps there main direction to `main.ts`. it is compile with extra config

### split
```
npm run split:build
```
read `angular.json`. in some apps there main direction to `main-split.ts`. it is compile with extra config

